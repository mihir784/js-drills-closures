let counter = 0;

function counterFactory() {
  return {
    increment: () => {
      counter++;
      return counter;
    },

    decrement: () => {
      counter--;
      return counter;
    },
  };
}

export default counterFactory;
