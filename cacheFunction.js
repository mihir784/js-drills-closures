let cache = {};

function cacheFunction(cb) {
  return (number) => {
    if (!cache[number]) {
      cache[number] = cb(number);
    }

    return cache[number];
  };
}

export default cacheFunction;
