function limitFunctionCallCount(cb, n) {
  let counter = 0;

  return () => {
    if (counter < n) {
      return cb(++counter);
    } else {
      return null;
    }
  };
}

export default limitFunctionCallCount;
