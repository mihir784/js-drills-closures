import counterFactory from "../counterFactory.js";

const counter = counterFactory();

console.log(counter.increment());
console.log(counter.decrement());
