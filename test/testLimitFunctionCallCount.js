import limitFunctionCallCount from "../limitFunctionCallCount.js";

function cb(counter) {
  return `Function call : ${counter}`;
}

const functionCall = limitFunctionCallCount(cb, 3);

console.log(functionCall());
functionCall();
console.log(functionCall());
functionCall();
console.log(functionCall());
