import cacheFunction from "../cacheFunction.js";

function fib(number) {
  if (number == 1 || number == 2) {
    return 1;
  }

  return fib(number - 1) + fib(number - 2);
}

const result = cacheFunction(fib);

console.log(result(40));
console.log(result(40));
console.log(result(40));
console.log(result(40));
